%global gettext_version 0.19.8
%global glib2_version 2.67.4
%global gtk3_version 3.19.12
%global libsoup_version 3.0
%global webkit2gtk_version 2.33.1

Name:	    	gnome-online-accounts
Version:    	3.48.0
Release:     	1
Summary:	    Single sign-on framework for GNOME
License:     	LGPLv2+
URL:		    https://wiki.gnome.org/Projects/GnomeOnlineAccounts
Source0:    	https://download.gnome.org/sources/gnome-online-accounts/3.48/%{name}-%{version}.tar.xz

Patch0:		    0001-google-Remove-Photos-support.patch

BuildRequires:	gcr-devel
BuildRequires:	pkgconfig(gio-2.0) >= %{glib2_version}
BuildRequires:	pkgconfig(glib-2.0) >= %{glib2_version}
BuildRequires:	pkgconfig(gobject-2.0) >= %{glib2_version}
BuildRequires:	pkgconfig(gtk+-3.0) >= %{gtk3_version}
BuildRequires:	pkgconfig(gobject-introspection-1.0)
BuildRequires:	gettext >= %{gettext_version}
BuildRequires:	gtk-doc
BuildRequires:	krb5-devel
BuildRequires:	meson
BuildRequires:	pkgconfig(webkit2gtk-4.1) >= %{webkit2gtk_version}
BuildRequires:	pkgconfig(json-glib-1.0)
BuildRequires:	pkgconfig(libsecret-1)
BuildRequires:	pkgconfig(libsoup-3.0) >= %{libsoup_version}
BuildRequires:	pkgconfig(rest-1.0)
BuildRequires:	pkgconfig(libxml-2.0)
BuildRequires:	vala

Requires:   	glib2%{?_isa} >= %{glib2_version}
Requires:   	gtk3%{?_isa} >= %{gtk3_version}
Requires:   	libsoup3%{?_isa} >= %{libsoup_version}
Requires:   	webkit2gtk4.1%{?_isa} >= %{webkit2gtk_version}

%description
GNOME Online Accounts provides interfaces so that applications and libraries
in GNOME can access the user's online accounts. It has providers for Google,
Nextcloud, Flickr, Foursquare, Microsoft Account, Microsoft Exchange, Fedora,
IMAP/SMTP and Kerberos.

%package devel
Summary:	Development files for %{name}
Requires:	%{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package help
Summary: Man pages for gnome-online-accounts

%description help
Man pages for gnome-online-accounts.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson \
  -Dexchange=true \
  -Dgoogle=true \
  -Dimap_smtp=true \
  -Dfedora=false \
  -Dkerberos=false \
  -Dlastfm=false \
  -Dmedia_server=false \
  -Downcloud=true \
  -Dwindows_live=true \
  -Dman=true \
  %{nil}

%meson_build

%install
%meson_install

%delete_la_and_a

%find_lang %{name}

%ldconfig_post
%ldconfig_postun

%files -f %{name}.lang
%license COPYING
%doc NEWS README
%dir %{_libdir}/girepository-1.0
%{_libdir}/girepository-1.0/Goa-1.0.typelib
%{_libdir}/libgoa-1.0.so.0
%{_libdir}/libgoa-1.0.so.0.0.0
%{_libdir}/libgoa-backend-1.0.so.1
%{_libdir}/libgoa-backend-1.0.so.1.0.0
%dir %{_libdir}/goa-1.0
%dir %{_libdir}/goa-1.0/web-extensions
%{_libdir}/goa-1.0/web-extensions/libgoawebextension.so
%{_prefix}/libexec/goa-daemon
#%%{_prefix}/libexec/goa-identity-service
%{_datadir}/dbus-1/services/org.gnome.OnlineAccounts.service
#%%{_datadir}/dbus-1/services/org.gnome.Identity.service
%{_datadir}/icons/hicolor/*/apps/goa-*.svg
%{_datadir}/glib-2.0/schemas/org.gnome.online-accounts.gschema.xml

%files devel
%{_includedir}/goa-1.0/
%{_libdir}/libgoa-1.0.so
%{_libdir}/libgoa-backend-1.0.so
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/Goa-1.0.gir
%{_libdir}/pkgconfig/goa-1.0.pc
%{_libdir}/pkgconfig/goa-backend-1.0.pc
%{_libdir}/goa-1.0/include
%{_datadir}/vala/

%files help
%{_datadir}/man/man8/goa-daemon.8*

%changelog
* Thu Nov 23 2023 lwg <liweiganga@uniontech.com> - 3.48.0-1
- update to version 3.48.0

* Mon Dec 5 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 3.46.0-1
- update to 3.46.0 for gnome 43

* Mon Jun 6 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 3.44.0-1
- update to 3.44.0

* Thu May 19 2022 loong_C <loong_c@yeah.net> - 3.43.1-3
- update gnome-online-accounts.spec

* Mon Mar 28 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 3.43.1-2
- update to 3.43.1-2

* Tue Jul 20 2021 liuyumeng <liuyumeng5@huawei.com> - 3.38.2-2
- delete gdb in buildrequires

* Mon May 24 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.2-1
- Upgrade to 3.38.2
- Update Version

* Fri Jan 29 2021 yanglu <yanglu60@huawei.com> - 3.38.0-1
- update to 3.38.0

* Fri Mar 20 2020 songnannan <songnannan2@huawei.com> - 3.30.0-5
- add gdb in buildrequires

* Wed Sep 18 2019 Zaiwang Li <lizaiwang1@huawei.com> - 3.30.0-4
- Init Package
